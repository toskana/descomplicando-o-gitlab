## Setup to test the converter

### Setting the MySQL server teste!

Create a container running MySQL
```bash
docker run --name=mysql1 -d -p 3306:3306 mysql/mysql-server
```

Get the root password:
```bash
docker logs mysql1 2>&1 | grep GENERATED
```

Connect into the MySQL:
```bash
docker exec -it mysql1 mysql -uroot -p
```

Change the root password:
```bash
ALTER USER 'root'@'localhost' IDENTIFIED BY 'MyNewPass';
```

Give permissions to connect remotely:
```bash
GRANT ALL PRIVILEGES ON *.* TO 'root'@'localhost' WITH GRANT OPTION;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;
flush privileges;
```

Create the database:
```bash
CREATE DATABASE converter;
```

Create the table and columns
```bash
CREATE  TABLE IF NOT EXISTS `list` ( `date` DATE , `name` LONGTEXT, `MD5` LONGTEXT, `converted` BOOL, `failed` BOOL, `name_converted` LONGTEXT, `md5_file_converted` LONGTEXT);
```

Show the items of the table:
```bash
USE converter;
SELECT * FROM list;
```

### Building images

Create image for both services, the videos list creator and the converter.
```bash
docker build -t converter_converter:1.0 --target converter .
docker build -t converter_list:1.0 --target list .
```

### Converting video files
 
To run it locally you just need to bind a directory with videos that you want to convert to a smaller size and 1080p, by default.

To generate the list of videos to convert:
```bash
docker run -ti -v /fullpath/of/the/directory_with_videos_to_be_converted/videos:/app/videos/ converter_list:1.0 --help               
```

To convert the videos that was include into the list:
```bash
docker run -ti -v /fullpath/of/the/directory_with_videos_to_be_converted/videos:/app/videos/ converter_list:1.0 --help               
```
